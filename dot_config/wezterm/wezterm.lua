-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This will hold the configuration.
local config = wezterm.config_builder()

-- This is where you actually apply your config choices
config.color_scheme = 'nord'
config.font = wezterm.font('JetBrains Mono NL', { weight = 'Regular', italic = false })
config.font_size = 10.0
config.initial_rows = 50
config.initial_cols = 120

-- and finally, return the configuration to wezterm
return config
